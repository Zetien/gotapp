from django.contrib import admin
from .models import Loan_application, Loan
# Register your models here.

@admin.register(Loan_application)
class Loan_applicationAdmin(admin.ModelAdmin):
    list_display = ['id','id_user','value', 'interest','description_loan']
@admin.register(Loan)
class LoanAdmin(admin.ModelAdmin):
    list_display = ['id','id_cobrador','id_request','balance','is_completed']
