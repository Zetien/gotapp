from django import forms
from .models import Loan_application, Loan
from users.models import User as Usuario


class LoanApplyForm(forms.ModelForm):
    description_loan = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))
    value = forms.FloatField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    fee_quantity = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    interest_v = Loan_application._meta.get_field('interest').get_default()
    interest = forms.FloatField(disabled=True, initial=interest_v,
                                widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Loan_application
        fields = ['description_loan', 'value', 'fee_quantity', 'interest']


class LoanForm(forms.ModelForm):
    id_cobrador = forms.ModelChoiceField(queryset=Usuario.objects.filter(role__id=3),
                                         widget=forms.Select(attrs={'class': 'form-control'}))
    comment = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}))

    class Meta:
        model = Loan
        fields = ['id_cobrador', 'comment']

    """ def save(self, commit=True):
        loan = super().save(commit=False)
        print(loan)
        Loan_application.objects.filter(pk=loan.id_request.id).update(comment=loan.comment)
        if commit:
            loan.save()
        return loan
 """
