from django.urls import path

from loans.views import loansAppView, loanAppCreate, loanAppDetail, LoansAppViews, loanView, loanAllView, loan_aff_view
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('list', loansAppView, name='index_loans_list'),
    path('addloans', loanAppCreate, name='add_apply_loan'),
    path('detail/<int:id_request>', loanAppDetail, name='detail_loan_app'),
    path('listall', login_required(LoansAppViews.as_view()), name='list_all_loans'),
    path('listloan', loanView, name='list_loan'),
    path('listloanall', loanAllView, name='list_loan_all'),
    path('listaffloans', loan_aff_view, name='list_aff_loans')
]


