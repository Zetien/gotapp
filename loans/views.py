from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, CreateView
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from users.models import User as Usuario

# Create your views here.
from users.models import Role
from .models import Loan_application, Loan
from .forms import LoanApplyForm, LoanForm


class LoansAppViews(ListView):
    model = Loan_application
    queryset = Loan_application.objects.all()
    template_name = 'loans/loans_view.html'
    context_object_name = 'index_loans_list'


@login_required(login_url='login')
def loanAppCreate(request):
    user = User.objects.get(username=request.user).usuario
    if request.method == 'POST':
        form = LoanApplyForm(request.POST)
        if form.is_valid():
            newform = form.save(commit=False)
            newform.id_user = user
            newform.save()
            return redirect('index_loans_list')
    else:
        form = LoanApplyForm()
    # TEA = Loan_application.objects.all()[0].interest
    context = {'form': form}
    return render(request, 'loans/add_apply_loans.html', context)


@login_required(login_url='login')
def loansAppView(request):
    user = User.objects.get(username=request.user).usuario
    loansApp = Loan_application.objects.filter(id_user=user)
    context = {'index_loans_list': loansApp}
    return render(request, 'loans/loans_view.html', context)


@login_required(login_url='login')
def loanAppDetail(request, id_request):
    # loanapp = Loan_application.objects.get(pk=id_request)
    loanapp = get_object_or_404(Loan_application, pk=id_request)

    if request.method == 'POST':
        form = LoanForm(request.POST)
        if form.is_valid():
            Loan_application.objects.filter(pk=id_request).update(comment=request.POST['comment'])
            Loan_application.objects.filter(pk=id_request).update(status_application=request.POST['status'])
            if request.POST['status'] == 'Aprobado':
                newform = form.save(commit=False)
                newform.id_request = loanapp
                newform.balance = round(loanapp.value * pow((1 + float(loanapp.interest / 12) / 100), loanapp.fee_quantity), 2)
                newform.num_quotas_left = loanapp.fee_quantity
                newform.save()
            return redirect('list_all_loans')
    else:
        form = LoanForm()
    context = {'request': loanapp, 'form': form}
    return render(request, 'loans/detail.html', context)


@login_required(login_url='login')
def loanView(request):
    user = User.objects.get(username=request.user).usuario
    loan = Loan.objects.filter(id_request__id_user=user)
    context = {'loan': loan}
    return render(request, 'loans/loan_view.html', context)


def loanAllView(request):
    loan = Loan.objects.all()
    context = {'loan': loan}
    return render(request, 'loans/loan_view.html', context)


def loan_aff_view(request):
    user = User.objects.get(username=request.user).usuario
    loan = Loan.objects.filter(id_cobrador=user)
    context = {'loan': loan}
    return render(request, 'loans/loan_view.html', context)


def handler_not_found(request, exception):
    return render(request, '404.html')
