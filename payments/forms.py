from django import forms

from loans.models import Loan
from payments.models import Payment


class PaymentForm(forms.ModelForm):
    value = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    # loan = forms.ModelChoiceField(widget=forms.Select(attrs={'class':'form-control'}))

    class Meta:
        model = Payment
        fields = ['value']


