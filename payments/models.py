from django.db import models
from loans.models import Loan


# Create your models here.
class Payment(models.Model):
    value = models.FloatField()
    date_payment = models.DateField(auto_now_add=True)
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.value}'
