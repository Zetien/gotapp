from django.urls import path

from payments.views import create_payments, list_payment

urlpatterns = [
    path('create/<int:id_request>', create_payments, name='create_payment'),
    path('listpayment/<int:id_request>', list_payment, name='list_payment')
]
