from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
# Create your views here.
from loans.models import Loan
from payments.forms import PaymentForm
from payments.models import Payment


def create_payments(request, id_request):
    # loan = Loan.objects.get(pk=id_request)
    loan = get_object_or_404(Loan, pk=id_request)
    quota_value = calculate_quota(loan.balance, loan.num_quotas_left, loan.id_request.interest, loan.id_request.value, loan.id_request.fee_quantity)

    if request.method == 'POST':
        form = PaymentForm(request.POST)
        if form.is_valid():
            if float(request.POST['value']) < quota_value:
                form.add_error('value', 'Enter at least the minimum quota value')
            elif float(request.POST['value']) == 0:
                form.add_error('value', 'You cannot pay 0')
            else:
                # Adjust balance
                new_balance = round(loan.balance - float(request.POST['value']), 2)

                if float(request.POST['value']) > loan.balance:
                    form.add_error('value', 'You cannot pay more than current balance')
                else:
                    Loan.objects.filter(pk=loan.id).update(balance=new_balance)
                    loan = get_object_or_404(Loan, pk=id_request)

                    # Adjust is_completed
                    if float(loan.balance) == 0.0:
                        Loan.objects.filter(pk=loan.id).update(is_completed=True)

                    # Adjust quota left
                    quotas_left = loan.num_quotas_left - int(float(request.POST['value']) / quota_value)
                    Loan.objects.filter(pk=loan.id).update(num_quotas_left=quotas_left)

                    newform = form.save(commit=False)
                    newform.loan = loan
                    newform.save()

                    print(Loan.objects.filter(pk=loan.id).values())
                    return redirect(f'/payments/listpayment/{id_request}')
    else:
        form = PaymentForm()
    context = {'request': loan, 'form': form, 'title': 'Create Payment', 'quota': quota_value}

    return render(request, 'payments/create_payment.html', context)


def list_payment(request, id_request):
    payment = Payment.objects.filter(loan_id=id_request)
    context = {'payment': payment}
    return render(request, 'payments/list_payment.html', context)


def calculate_quota(balance, quotas_remaining, interest, value_request, fee_quantity):
    new_try = quotas_remaining - 1
    future_value = value_request * (1 + float(interest / 12) / 100) ** (fee_quantity - new_try)

    if fee_quantity - new_try > 1:
        previous_future = value_request * (1 + float(interest / 12) / 100) ** (fee_quantity - new_try - 1)
    else:
        previous_future = value_request

    new_interest = round(future_value - previous_future, 2)
    without_interest = value_request / fee_quantity
    new_quota = without_interest + new_interest

    if new_quota > balance:
        new_quota = balance

    return round(new_quota, 2)
    # return new_interest







    # interes_value = float(interest / 12) / 100
    # print(interes_value)
    # quota_value = balance * (1 + interes_value) / quotas_remaining
    # return round(quota_value, 2)

    # mes = balance / 12
    # inter = balance * (interest / 100) / 12
    # quota_value = mes + inter
    # return round(quota_value, 2)

