from django.contrib import admin
from .models import User, Role

# Register your models here
admin.site.register(User)
# admin.site.register(Role)


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    ordering = ['id']
    list_display = ('id', 'description_role')
