from django.db import models
from django.contrib.auth.models import AbstractBaseUser, User as UserAuth


# Create your models here
class Role(models.Model):
    description_role = models.CharField(max_length=200)

    def __str__(self):
        return self.description_role


class User(models.Model):
    user = models.OneToOneField(UserAuth, on_delete=models.CASCADE, related_name='usuario')
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    ID_number = models.IntegerField(unique=True)
    address = models.CharField(max_length=200)
    phone = models.IntegerField()
    email = models.EmailField()
    role = models.ForeignKey(Role, on_delete=models.CASCADE)  # limit_choices_to=Q(id=2) | Q(id=3),

    def __str__(self):
        return self.name + self.last_name



