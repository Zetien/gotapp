from django.urls import path, include
from users import views

urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register_user, name='register_user'),
    path('list/', views.list_user, name='list_user'),
    # path('login/', views.login_user, name='login_user'),
    # path('logout/', views.logout_user, name='logout_user'),
    # path('profile/', views.profile_user, name='profile_user')
]


