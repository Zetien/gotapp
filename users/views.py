from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User as UserAuth

# Create your views here.
from users.models import User
from users.forms import CreateUserForm
from users.decorators import unauthenticated_user, allowed_users


@login_required(login_url='login')
@allowed_users(allowed_roles=['prestamista', 'cliente', 'cobrador', 'admin'])
def home(request):
    context = {}
    return render(request, 'base.html', context)


@unauthenticated_user
def register_user(request):
    """
    Create and save a user
    """
    title = "Sign up"

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
    else:
        form = CreateUserForm()

    context = {
        'form': form,
        'title': title
    }
    return render(request, 'users/register.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def list_user(request):
    """
    List all users
    """
    context = User.objects.all()
    return render(request, 'users/list.html', {'users': context})


# @unauthenticated_user
# def login_user(request):
#     """
#     Login and authentication of a user
#     """
#     title = "Log in"
#
#     context = {
#         'title': title
#     }
#
#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password')
#
#         user = authenticate(request, username=username, password=password)
#
#         if user is not None:
#             login(request, user)
#             return redirect('home')
#         # else:
#         #     messages.info(request, 'Username OR password is wrong')
#
#     return render(request, 'users/login.html', context)
#
#
# def logout_user(request):
#     """
#     Logout an authenticated user
#     """
#     logout(request)
#     return redirect('login_user')
#
#
# @login_required(login_url='login')
# @allowed_users(allowed_roles=['prestamista', 'cliente', 'cobrador'])
# def profile_user(request):
#     """
#     Get details of a user
#     """
#     context = {
#         'title': 'Profile',
#     }
#     return render(request, 'users/profile.html', context)


